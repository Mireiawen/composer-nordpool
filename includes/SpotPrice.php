<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

use DateTime;

/**
 * Electricity spot price handler
 *
 * @package Mireiawen\Nordpool
 */
class SpotPrice
{
	/**
	 * The start time
	 *
	 * @var DateTime
	 */
	protected DateTime $start;
	
	/**
	 * The price area
	 *
	 * @var Area
	 */
	protected Area $area;
	
	/**
	 * The price currency
	 *
	 * @var Currency
	 */
	protected Currency $currency;
	
	/**
	 * The spot price value
	 *
	 * @var float
	 */
	protected float $value;
	
	/**
	 * The tax percentage
	 *
	 * @var float
	 */
	protected float $tax;
	
	/**
	 * The spot price value with the tax added
	 *
	 * @var float
	 */
	protected float $taxed_value;
	
	/**
	 * @param DateTime $start
	 *    The start time
	 *
	 * @param Area $area
	 *    The price area
	 *
	 * @param Currency $currency
	 *    The price currency
	 *
	 * @param float $value
	 *    The spot price value
	 *
	 * @param float $tax
	 *    The tax percentage
	 *
	 * @param float $taxed_value
	 *    The spot price value with the tax added
	 */
	public function __construct(DateTime $start, Area $area, Currency $currency, float $value, float $tax = 0.0, float $taxed_value = 0.0)
	{
		$this->start = $start;
		$this->area = $area;
		$this->currency = $currency;
		$this->value = $value;
		$this->tax = $tax;
		$this->taxed_value = $taxed_value;
	}
	
	/**
	 * Get the start time
	 *
	 * @return DateTime
	 */
	public function GetStart() : DateTime
	{
		return $this->start;
	}
	
	/**
	 * Get the area
	 *
	 * @return Area
	 */
	public function GetArea() : Area
	{
		return $this->area;
	}
	
	/**
	 * Get the currency
	 *
	 * @return Currency
	 */
	public function GetCurrency() : Currency
	{
		return $this->currency;
	}
	
	/**
	 * Get the spot price value
	 *
	 * @return float
	 */
	public function GetValue() : float
	{
		return $this->value;
	}
	
	/**
	 * Get the tax
	 *
	 * @return float
	 */
	public function GetTax() : float
	{
		return $this->tax;
	}
	
	/**
	 * Get the spot price value with the tax added
	 *
	 * @return float
	 */
	public function GetValueWithTax() : float
	{
		return $this->taxed_value;
	}
}