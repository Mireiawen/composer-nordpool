<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool\Error;

use RuntimeException;
use Throwable;
use function sprintf;

/**
 * API error exception
 *
 * @package Mireiawen\Nordpool\Error
 */
class API extends RuntimeException
{
	/**
	 * The exception constructor
	 *
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
	public function __construct(string $message, int $code = 0, ?Throwable $previous = NULL)
	{
		if (!$previous !== NULL)
		{
			$message = sprintf('%s: %s', $message, $previous->getMessage());
		}
		parent::__construct($message, $code, $previous);
	}
}