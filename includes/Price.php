<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;
use Mireiawen\Nordpool\Error\API;
use Mireiawen\Nordpool\Error\DateTime;
use function _;
use function json_decode;
use function str_replace;

/**
 * Nordpool price fetcher
 *
 * @package Mireiawen\Nordpool
 */
class Price extends Nordpool
{
	/**
	 * To convert MWh to KWh
	 *
	 * @var float
	 */
	protected const MWh_to_KWh = 0.001;
	
	/**
	 * The area to use
	 *
	 * @var Area
	 */
	protected Area $area;
	
	/**
	 * The timezone to use
	 *
	 * @var DateTimeZone
	 */
	
	protected DateTimeZone $timezone;
	/**
	 * The tax multiplier
	 *
	 * @var float
	 */
	protected float $tax;
	
	/**
	 * The tax value
	 *
	 * @var float
	 */
	protected float $tax_value;
	
	/**
	 * The currency to use
	 *
	 * @var Currency
	 */
	protected Currency $currency;
	
	/**
	 * Initialize the class and set its parameters
	 *
	 * @param Area $area
	 *    The Area to fetch the prices from
	 *
	 * @param DateTimeZone|null $timezone
	 *    The timezone to use, if unset will use UTC
	 *
	 * @param float $tax
	 *    The tax rate, in percent, for example 24.0
	 *    Set to 0 to not calculate taxes
	 *
	 * @param Currency $currency
	 *    The currency to request
	 */
	public function __construct(Area $area, ?DateTimeZone $timezone, float $tax = 0.00, Currency $currency = Currency::EUR)
	{
		if ($timezone === NULL)
		{
			$timezone = new DateTimeZone('UTC');
		}
		
		$this->area = $area;
		$this->tax = 1.00 + $tax / 100.0;
		$this->tax_value = $tax;
		$this->currency = $currency;
		$this->timezone = $timezone;
		parent::__construct();
	}
	
	/**
	 * @param Report $type
	 *    The type of the report to get
	 *
	 * @param DateTimeInterface|null $from
	 *    The start time for the data to get
	 *
	 * @param DateTimeInterface|null $to
	 *    The end time for the data to get
	 *
	 * @return SpotPrice[]
	 *    Array of spot prices
	 *
	 * @throws DateTime
	 *    In case of PHP Date / Time errors
	 *
	 * @throws API
	 *    In case of API errors
	 */
	public function Fetch(Report $type, ?DateTimeInterface $from = NULL, ?DateTimeInterface $to = NULL) : array
	{
		// Get the CET time
		$cet = new DateTimeZone('Europe/Berlin');
		
		// Create the end time, as it is needed for the API
		if ($to === NULL)
		{
			try
			{
				$to = new DateTimeImmutable('now', $this->timezone);
			}
			catch (Exception $exception)
			{
				throw new DateTime(_('Unable to create an instance of DateTimeImmutable'), 0, $exception);
			}
		}
		
		// Change the end date to CET time zone
		$end_cet = clone $to;
		$end_cet->setTimezone($cet);
		
		// Read the API
		try
		{
			$response = $this->Request(\sprintf('page/%s', $type->value), ['currency' => $this->currency->value, 'endDate' => $end_cet->format('d-m-Y')]);
		}
		catch (GuzzleException $exception)
		{
			throw new API(_('Request failed'), 0, $exception);
		}
		
		// Try to parse the API response as JSON
		try
		{
			$json = json_decode((string)$response->getBody(), TRUE, 512, JSON_THROW_ON_ERROR);
		}
		catch (JsonException $exception)
		{
			throw new API(_('JSON parsing failed'), 0, $exception);
		}
		
		// Go through the data rows
		$rows = $json['data']['Rows'] ?? [];
		$data = [];
		foreach ($rows as $row)
		{
			try
			{
				$start = new \DateTime($row['StartTime'], $cet);
				$end = new \DateTime($row['EndTime'], $cet);
			}
			catch (Exception $exception)
			{
				throw new DateTime(_('Unable to create an instance of DateTime'), 0, $exception);
			}
			$start->setTimezone($this->timezone);
			$end->setTimezone($this->timezone);
			
			// Skip the entries we did not want
			if ($start < $from || $end > $to)
			{
				continue;
			}
			
			// Skip the extra data
			if ($row['IsExtraRow'] === TRUE)
			{
				continue;
			}
			
			foreach ($row['Columns'] as $column)
			{
				// Fetch for the area we requested
				if ($column['Name'] !== $this->area->value)
				{
					continue;
				}
				
				// Skip non-official rows for now
				if ($column['IsOfficial'] !== TRUE)
				{
					continue;
				}
				
				// Get the value, it is by default currency/MWh, convert it to kWh which is more useful for home
				$value = (float)str_replace(',', '.', $column['Value']) * self::MWh_to_KWh;
				$taxed_value = $value * $this->tax;
				
				// Add the created price point to array
				$spot_price = new SpotPrice($start, $this->area, $this->currency, $value, $this->tax_value, $taxed_value);
				$data[] = $spot_price;
			}
		}
		
		return $data;
	}
}
