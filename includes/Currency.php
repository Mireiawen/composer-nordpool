<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

/**
 * All supported currencies
 *
 * @package Mireiawen\Nordpool
 */
enum Currency: string
{
	case EUR = 'EUR';
	case NOK = 'NOK';
	case DKK = 'DKK';
	case SEK = 'SEK';
}