<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

/**
 * The report types
 *
 * @package Mireiawen\Nordpool
 */
enum Report: string
{
	case Hourly = '10';
	case Daily = '11';
	case Weekly = '12';
	case Monthly = '13';
	case Yearly = '14';
}