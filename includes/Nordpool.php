<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

/**
 * The Nordpool data fetcher
 *
 * @package Mireiawen\Nordpool
 */
class Nordpool
{
	/**
	 * The API base URL
	 *
	 * @var string
	 */
	final protected const BASE_URI = 'https://www.nordpoolspot.com/api/marketdata/';
	
	/**
	 * HTTP client
	 *
	 * @var Client
	 */
	protected Client $client;
	
	/**
	 * The constructor for Nordpool
	 */
	public function __construct()
	{
		$this->client = new Client(['base_uri' => self::BASE_URI]);
	}
	
	/**
	 * @param string $path
	 *    The request path
	 *
	 * @param array $params
	 *    The request options, if any
	 *
	 * @param string $method
	 *    The HTTP method to use
	 *
	 * @return ResponseInterface
	 *    HTTP response
	 *
	 * @throws GuzzleException
	 *    In case of HTTP errors
	 */
	protected function Request(string $path, array $params = [], string $method = 'GET') : ResponseInterface
	{
		return $this->client->request($method, $path, [RequestOptions::QUERY => $params]);
	}
}