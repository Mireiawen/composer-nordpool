<?php
declare(strict_types = 1);

namespace Mireiawen\Nordpool;

/**
 * Nordpool area
 *
 * @see https://www.nordpoolgroup.com/en/Market-data1/#/nordic/map
 *
 * @package Mireiawen\Nordpool
 */
enum Area: string
{
	case Austria = 'AT';
	case Belgium = 'BE';
	case Denmark1 = 'DK1';
	case Denmark2 = 'DK2';
	case Estonia = 'EE';
	case Finland = 'FI';
	case France = 'FR';
	case Germany = 'DE';
	case GreatBritain = 'GB';
	case Latvia = 'LV';
	case Lithuania = 'LT';
	case Netherlands = 'NL';
	case Norway1 = 'NO1';
	case Norway2 = 'NO2';
	case Norway3 = 'NO3';
	case Norway4 = 'NO4';
	case Norway5 = 'NO5';
	case Poland = 'PL';
	case Sweden1 = 'SE1';
	case Sweden2 = 'SE2';
	case Sweden3 = 'SE3';
	case Sweden4 = 'SE4';
	case SYS = 'SYS';
}